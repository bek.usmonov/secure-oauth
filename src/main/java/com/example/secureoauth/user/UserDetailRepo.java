package com.example.secureoauth.user;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserDetailRepo extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String name);
}
